from django.apps import AppConfig


class RefurbishedConfig(AppConfig):
    name = 'refurbished'
