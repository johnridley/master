from django.db import models

class Type(models.Model):
	name = models.CharField(max_length=50)
	displayName = models.CharField(max_length=50)
	displaySize = models.CharField(max_length=50)
	imagename = models.CharField(max_length=50)

	def __str__(self):
		return self.name

class Feature(models.Model):
	name = models.CharField(max_length=50)

	def __str__(self):
		return self.name

class Processor(models.Model):
	name = models.CharField(max_length=50)
	speed = models.FloatField()
	cores = models.IntegerField()

	def __str__(self):
		return f'{str(self.speed)}GHz {self.name} - {str(self.cores)} core'

class Memory(models.Model):
	size = models.IntegerField()
	speed =  models.IntegerField()
	form = models.CharField(max_length=50)

	def __str__(self):
		return f'{str(self.size)}GB {self.form} {str(self.speed)}MHz'

class Drive(models.Model):
	size = models.IntegerField()
	new = models.BooleanField()
	form = models.CharField(max_length=50)

	def __str__(self):
		return f'{"New" if self.new else ""} {str(self.size)}GB {self.form}'


class Build(models.Model):
	productCode = models.CharField(unique=True, max_length=50)
	avaliable = models.BooleanField()
	price = models.IntegerField()

	model = models.ForeignKey(Type, on_delete=models.CASCADE)
	processor = models.ForeignKey(Processor, on_delete=models.CASCADE)
	memory = models.ForeignKey(Memory, on_delete=models.CASCADE)
	drive = models.ForeignKey(Drive, on_delete=models.CASCADE)

	features = models.ManyToManyField(Feature)

	def save(self, *args, **kwargs):
		self.productCode = self.productCode.upper()
		return super(Build, self).save(*args, **kwargs)

	def __str__(self):
		return self.productCode

