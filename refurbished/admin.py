from django.contrib import admin
from django.utils.safestring import mark_safe

from .models import *

admin.site.site_header = 'MASTER'
admin.site.register(Type)
admin.site.register(Feature)
admin.site.register(Processor)
admin.site.register(Memory)
admin.site.register(Drive)

class FeatureInline(admin.StackedInline):
	model = Build.features.through
	extra = 0

class BuildAdmin(admin.ModelAdmin):
	def pdf_link(self):
		return mark_safe(f"<a href='/refurbished/pdf/{self.productCode}'>Generate PDF</a>")

	pdf_link.short_description = ''

	exclude = ['features']
	inlines = [FeatureInline]
	list_display = ('productCode', 'avaliable', 'model', 'processor', 'memory', 'drive', 'price', pdf_link)
	save_as = True

admin.site.register(Build, BuildAdmin)
