from django.urls import path

from . import views

urlpatterns = [
	path('', views.index, name='index'),
	path('device/<str:productCode>', views.detail, name="detail"),
	path('pdf/<str:productCode>', views.generatePDF, name="pdf"),
]


