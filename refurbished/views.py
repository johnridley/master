from django.shortcuts import render, redirect
from django.template import loader
from django.http import HttpResponse

from weasyprint import HTML

from .models import Build

def index(request):
	builds = Build.objects.filter(avaliable=True).order_by('price')
	context = {'buildList' : builds}
	return render(request, 'refurbished/index.html', context)

def detail(request, productCode):
	build = Build.objects.get(productCode__iexact=productCode)
	context = {'build' : build}
	return render(request, 'refurbished/build.html', context)

def notfound(request, exception):
	return redirect('/refurbished')

def generatePDF(request, productCode):
	build = Build.objects.get(productCode__iexact=productCode)
	template = loader.get_template('refurbished/pdf.html')
	context = {'build' : build}
	pdf = HTML(string=template.render(context, request)).write_pdf()
	response = HttpResponse(pdf, content_type='application/pdf')
	response['Content-Disposition'] = 'filename="test.pdf"'
	return response

