# MASTER - Macs4U Auxiliary System for Tracking External Resources

## Setup

### Install Dependencies

	brew install nginx python3 cairo pango gdk-pixbuf libxml2 libxslt libffi

	pip3 install django weasyprint

	sudo mkdir /usr/local/etc/nginx/sites-enabled

### Setup Nginx

Edit /usr/local/etc/nginx/nginx.conf
	
	http {
    include /usr/local/etc/nginx/sites-enabled/*;
	...

Symlink nginx.conf

	ln -s /usr/local/etc/nginx/sites-enabled/master nginx.conf

### Setup Static Files

	mkdir logs
	mkdir static
	#Recursive permissions
	chmod 777 (directories to static)
	python3 manage.py collectstatic

### Set Correct Paths in 

	gunicorn.conf.py
	nginx.conf
	com.macs4u.gunicorn.plist

### Setup launchd settings

	sudo cp com.macs4u.nginx.plist /Library/LaunchDaemons/.
	sudo cp com.macs4u.gunicorn.plist /Library/LaunchDaemons/.
	sudo launchctl load -w /Library/LaunchDaemons/com.macs4u.nginx.plist
	sudo launchctl load -w /Library/LaunchDaemons/com.macs4u.gunicorn.plist

## Check Status

	sudo launchctl list | grep macs4u

## Manual Operation

	sudo nginx
	gunicorn -c gunicorn.conf.py -b '127.0.0.1:9000' master.wsgi

## launchd Operation

	sudo launchctl start com.macs4u.nginx
	sudo launchctl start com.macs4u.gunicorn	
	
	sudo launchctl stop com.macs4u.nginx
	sudo launchctl stop com.macs4u.gunicorn

	