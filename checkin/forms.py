from django import forms

class CheckInForm(forms.Form):
	document = forms.CharField(required=False, max_length=50, widget=forms.TextInput(attrs={'pattern':'\d*'}))
	customerName = forms.CharField(required=False, max_length=50)
	customerAddress = forms.CharField(required=False, max_length=200)
	customerTelephone = forms.CharField(required=False, max_length=20, widget=forms.TextInput(attrs={'pattern':'\d*'}))
	customerEmail = forms.EmailField(required=False)

	machineModel = forms.CharField(required=False, max_length=50)
	machineSerial = forms.CharField(required=False, max_length=50)
	problem = forms.CharField(required=False, widget=forms.Textarea)
	action = forms.CharField(required=False, widget=forms.Textarea)
	signature = forms.CharField(required=False, label='', widget=forms.HiddenInput())

	