from django.contrib import admin

from .models import *

class CheckInAdmin(admin.ModelAdmin):
	readonly_fields = ('signatureImage',)
	list_display = ('document', 'customerName', 'customerTelephone', 'machineModel', 'machineSerial')

admin.site.register(CheckIn, CheckInAdmin)