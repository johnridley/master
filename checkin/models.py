from django.db import models
from django.utils.safestring import mark_safe

class CheckIn(models.Model):
	document = models.CharField(max_length=50)
	customerName = models.CharField(max_length=50)
	customerAddress = models.CharField(max_length=200)
	customerTelephone = models.CharField(max_length=20)
	customerEmail = models.EmailField()

	machineModel = models.CharField(max_length=50)
	machineSerial = models.CharField(max_length=50)
	problem = models.TextField()
	action = models.TextField()
	signature = models.TextField()
	date = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.document

	def signatureImage(self):
		return mark_safe(f'<img src="{self.signature}"/>')

