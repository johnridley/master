from django.shortcuts import render

from .forms import CheckInForm
from .models import CheckIn

def index(request):
	error = False
	success = False
	if request.method == "POST":
		form = CheckInForm(request.POST)
		print(form.errors)
		if form.is_valid():
			signature = CheckIn()
			if form.cleaned_data['document'] == "":
				form.cleaned_data['document'] = "No SRO"
			signature.document = form.cleaned_data['document']
			signature.customerName = form.cleaned_data['customerName']
			signature.customerAddress = form.cleaned_data['customerAddress']
			signature.customerTelephone = form.cleaned_data['customerTelephone']
			signature.customerEmail = form.cleaned_data['customerEmail']
			signature.machineModel = form.cleaned_data['machineModel']
			signature.problem = form.cleaned_data['problem']
			signature.action = form.cleaned_data['action']
			signature.signature = form.cleaned_data['signature']
			signature.save()
			success = True
		else:
			error = True


	form = CheckInForm()
	context = {'form' : form, 'success' : success, 'error' : error}
	return render(request, 'checkin/checkin.html', context)
